clc
close all
clear all

%% Initialize Parameters & Variables
ep = 0.1;
Q = zeros(4,12,4);

TotalReturn = [];
alpha = 0.1;
gamma = 1;

%% On-Policy Monte Carlo Method
for TrainLoop = 1:500
    %% Generating an Episode
    EpisodeRewards = [];
    count = 0;
    Episodend = 0;
    Cstate.x = 4;  Cstate.y = 1;
    
    %% epsilon-Greedy Policy
    [~,index] = max(Q(Cstate.x,Cstate.y,:));
    temp = ones(1,4)*ep/4;
    temp(index) = temp(index) + (1-ep);
    roulette = cumsum(temp);
    rand_temp = rand;
    CAction = sum(roulette <= rand_temp)+1;
    
    while Episodend == 0
        count = count+1;
        
        %% Interaction   
        [Nstate,reward,Episodend] = CliffWord(Cstate,CAction);
        
        %% epsilon-Greedy Policy
        [~,index] = max(Q(Nstate.x,Nstate.y,:));
        temp = ones(1,4)*ep/4;
        temp(index) = temp(index) + (1-ep);
        roulette = cumsum(temp);
        rand_temp = rand;
        NAction = sum(roulette <= rand_temp)+1;
        
        %% Updating Q(s,a)
        deltaQ = alpha*( reward + gamma*Q(Nstate.x,Nstate.y,NAction) - Q(Cstate.x,Cstate.y,CAction) );
        Q(Cstate.x,Cstate.y,CAction) = Q(Cstate.x,Cstate.y,CAction)+deltaQ;
        
        %%
        EpisodeRewards = [EpisodeRewards; reward];
        
        Cstate  = Nstate;
        CAction = NAction;
    end  
    %%
% 	ep = ep/1.01;
    
    TotalReturn = [TotalReturn; sum(EpisodeRewards(1:count));];
    disp(['Iteration ' num2str(TrainLoop) ': Episode Length ' num2str(count)])
    
end

%%
WorldPlot(Q)

win = 10;
n = length(TotalReturn);
temp = [0; cumsum(TotalReturn)];
ind  = max(1, (1:n+1) - (win-1));
TotalReturn_f = (temp - temp(ind)) ./ ((1:n+1) - ind)';
TotalReturn_f = TotalReturn_f(2:end);
figure(2); plot(TotalReturn,'y'); grid on
hold on; plot(TotalReturn_f,'r');
