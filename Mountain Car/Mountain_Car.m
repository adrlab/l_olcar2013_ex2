function [x1,v1,r,EpisodeEnd] = Mountain_Car(x,v,a)

%-- Mountain Car benchmark --%
% Ref. paper: "Fitted Natural Actor-Critic: A New Algorithm for Continuous
%              State-Action MDPs"
%
% [x1,v1,r,EpisodeEnd] = Mountain_Car(x,v,a)
% x: current position in the range [-1.2,+0.5]
% v: current velocity in the range [-0.07,+0.07]
% a: acceleration in the range [-1,+1]
% x1: next position in the range [-1.2,+0.5]
% v1: next velocity in the range [-0.07,+0.07]
% r: reward. 10 if car reaches x=0.5, -1 otherwise.
% EpisodeEnd: flag which toggle 1 at the end of episode (i.e. x=+0.5)

r = -1;
EpisodeEnd = 0;
a = (abs(a)<=1)*a + (abs(a)>1)*sign(a);

v1 = v + 0.001*a -0.0025*cos(3*x);
v1 = (v1<=+0.07)*v1+(v1>+0.07)*0.07;
v1 = (v1>=-0.07)*v1+(v1<-0.07)*-0.07;

x1 = x + v1;
if x1 <= -1.2
    x1 = -1.2;
    v1 = 0;
end
if x1 >= 0.5
    x1 = 0.5;
    v1 = 0;
    r = 10;
    EpisodeEnd = 1;
end
