clc
close all
clear all

%% Tiling (Specify feature space)
Num_Tile_x = 20;
Num_Tile_v = 20;
w1 = (0.50 - (-1.20))/Num_Tile_x;
w2 = (0.07 - (-0.07))/Num_Tile_v;
i=1;

StateN = Num_Tile_x*Num_Tile_v;
TileCenter = zeros(StateN,2);
for k=1:Num_Tile_x
    for l=1:Num_Tile_v
        TileCenter(i,1) = -1.20 + (k-1)*w1 + w1/2;
        TileCenter(i,2) = -0.07 + (l-1)*w2 + w2/2;
        i=i+1;
    end
end

%% ****   Learning Algorithm   **** %%

%% Initialization
% epsilone = 0.1;
lambda  = 0.5;
gamma   = 1;
epsilon = 0.5;
alpha   = 0.1;
Q  = zeros(StateN,3);

MaxIteration = 1200;
MaxTime  = 500;

%% main loop
count = 0;
AllaccR  = zeros(MaxIteration,1);
while count < MaxIteration
    %% Episode Initializatin
    epsilon = epsilon*0.995;
    count = count +1;
    
    e  = zeros(StateN,3);
    x = -pi/6;
    v = 0;
    a = 1;
    
    %Find Features
    temp = repmat([x,v],StateN,1);
    Dist = sum((temp-TileCenter).^2,2);
    [~,state] = min(Dist);
%     disp([x v -1 state])
    
    Time = 0;
    finish = 0;
    
    Xstar = zeros(MaxTime,1);
    Vstar = zeros(MaxTime,1);
    Astar = zeros(MaxTime,1);
    
    %% for each stepsilon of epsilonisode
    while finish ~= 1 && Time <= MaxTime
        Time = Time+1;
        
        % Take Action
        switch a
            case 1;   action = -1;
            case 2;   action = 0;
            case 3;   action = +1;
        end
        [x1,v1,r,finish] = Mountain_Car(x,v,action);
        Xstar(Time) = x;
        Vstar(Time) = v;
        Astar(Time) = action;
        Astar(Time) = action;
        AllaccR(count) = AllaccR(count) + r;
        x = x1;  v = v1;
        
        % Find Feature
        temp = repmat([x,v],StateN,1);
        Dist = sum((temp-TileCenter).^2,2);
        [~,state_p] = min(Dist);

        
        % epsilon-greedy Policy
        [~,a_temp] = max(Q(state_p,:));
        temp = ones(1,3)*epsilon/3;
        temp(a_temp) = temp(a_temp)+(1-epsilon);
        roulette = cumsum(temp);
        rand_temp = rand;
        a_p = sum(roulette <= rand_temp)+1;
        
        % Computing the increment
        delta = r + gamma*Q(state_p,a_p) - Q(state,a);
        
        % Updating the Eligibility Trace
        e(state,a) = e(state,a)+1;
   
        Q = Q + alpha*delta*e;            
        e = lambda*gamma*e;
        
        state = state_p;
        a     = a_p;

        
    end
    
    if finish == 1
        delta = r + gamma*max(Q(state,:))- Q(state,a);
        
        % Updating the Eligibility Trace
        e(state,a) = e(state,a)+1;
   
        Q = Q + alpha*delta*e;            
        e = lambda*gamma*e;
        
    end
    Time = Time - 1;
    switch a
        case 1;   action = -1;
        case 2;   action = 0;
        case 3;   action = +1;
    end
    Xstar = [Xstar(1:Time); x];
    Vstar = [Vstar(1:Time); v];
    Astar = [Astar(1:Time); action];
    
    
    temp = max(Q,[],2);
    temp = reshape(temp,[Num_Tile_v,Num_Tile_x])';
    figure(4)
    contourf(temp)
    drawnow
    disp(['Episode ' num2str(count), ',   Time: ' num2str(Time)])
    
%     if mod(count,400) == 0 || count == 1
%         mov = visualizeMountainCar(50, Xstar, Vstar, Astar);
%         movie2avi(mov, ['MCar_animation_' num2str(count)])
%     end
            

end

%% Plottings
visualizeMountainCar(50, Xstar, Vstar, Astar);

win = 30;
n = length(AllaccR);
temp = [0; cumsum(AllaccR)];
ind  = max(1, (1:n+1) - (win-1));
AllaccR_f = (temp - temp(ind)) ./ ((1:n+1) - ind)';
AllaccR_f = AllaccR_f(2:end);
% n = length(AllaccR);
% AR = zeros(n,1);
% for l = 1:n
%     AR(l) = sum(AllaccR(1:l))/l;
% end 
figure(3); plot(AllaccR,'y'); grid on
hold on; plot(AllaccR_f,'r');


