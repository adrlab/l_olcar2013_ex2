function [Nstate,r,Episodend] = CliffWord(Cstate,Action)
%
% Cliff Walking model
%
% Cstate: Current state position structure
%                   .x: position in vertical axis with origin on top left
%                   .y: position in horizontal axis with origin on top left
% Action: Chosen action in the set {1,2,3,4} with convention
%           1: move one step up
%           2: move one step right
%           3: move one step down
%           4: move one step left
%
% Nstate: Next state position structure
% r: Reward
% Episodend: Flag which indicates the end of an episode
%            1: Agent has reached the goal position
%            0: Agent is still playing


r = -1;
Episodend = 0;
Nstate.x = 0;
Nstate.y = 0;

switch Action
    case 1
        Nstate.x = Cstate.x-1;
        Nstate.y = Cstate.y;
    case 2
        Nstate.y = Cstate.y+1;
        Nstate.x = Cstate.x;
    case 3
        Nstate.x = Cstate.x+1;
        Nstate.y = Cstate.y;
    case 4
        Nstate.y = Cstate.y-1;
        Nstate.x = Cstate.x;
end

if Nstate.x < 1 || Nstate.x > 4
    Nstate.x = Cstate.x;
end
if Nstate.y < 1 || Nstate.y > 12
    Nstate.y = Cstate.y;
end

if Nstate.x == 4
    if Nstate.y > 1 && Nstate.y < 12
        Nstate.y = 1;
        r = -100;
    end
    if Nstate.y == 12
        Episodend = 1;
        r = -1;
    end
end
